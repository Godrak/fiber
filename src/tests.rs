use super::Fiber;

#[test]
fn it_works() {
    assert_eq!(2 + 2, 4);
}

struct Position {
    pos: usize,
    rot: usize,
}

#[test]
fn test() {
    let mut fiber = Fiber::default();

    let e1 = fiber.create_entity();
    let e2 = fiber.create_entity();

    assert!(fiber.exists(e1));
    assert!(fiber.has::<Position>(e2).is_ok());

    let pos2 = Position { pos: 2, rot: 2 };
    let ires2 = fiber.set(e2, pos2);

    assert!(ires2.is_ok() && ires2.expect("msg") == 0);

    let pos1 = Position { pos: 1, rot: 1 };
    let ires1 = fiber.set(e1, pos1);

    assert!(ires1.is_ok() && ires1.expect("msg") == 1);

    assert!(fiber.borrow::<Position>(e1).unwrap().pos == 1);
    fiber.borrow_mut::<Position>(e1).unwrap().pos = 0;
    assert!(fiber.borrow::<Position>(e1).unwrap().pos == 0);
}
