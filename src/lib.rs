use std::any::Any;
use std::any::TypeId;
use std::collections::{HashMap, VecDeque, HashSet};
use std::mem;
use std::slice;
use std::mem::transmute;

type IdNumber = u64;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct EntityId(IdNumber);

#[derive(Debug, PartialEq, Eq)]
pub enum NotFound {
    Entity(EntityId),
    Component(TypeId),
}

pub type EcsResult<T> = Result<T, NotFound>;

pub trait Component: Any + Sized {}

impl<T: Any + Sized> Component for T {}

trait Data: Any {
    fn get_memory_view(&self) -> &[u8];

    //StackOverlow: How to get a reference to a concrete type from a trait object
    fn as_any(&self) -> &dyn Any;

    fn as_any_mut(&mut self) -> &mut dyn Any;
}

impl<T: Component> Data for Vec<T> {
    fn get_memory_view(&self) -> &[u8] {
        let data_pointer: *const u8 = &self[0] as *const T as *const u8; //first cast to pointer then to pointer to bytes, cant cast directly
        unsafe { slice::from_raw_parts(data_pointer, mem::size_of::<T>() * self.len()) }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

struct Storage {
    data: Box<dyn Data>,
    free_positions: VecDeque<usize>,
}

impl Storage {
    pub fn new<C: Component>() -> Self {
        Storage {
            data: Box::new(Vec::<C>::new()),
            free_positions: VecDeque::default(),
        }
    }

    fn cast_mut<C: Component>(&mut self) -> &mut Vec<C> {
        self.data.as_any_mut().downcast_mut::<Vec<C>>().unwrap()
    }

    fn cast<C: Component>(&self) -> &Vec<C> {
        self.data.as_any().downcast_ref::<Vec<C>>().unwrap()
    }

    pub fn get_memory_view(&self) -> &[u8] {
        self.data.get_memory_view()
    }

    pub fn insert<C: Component>(&mut self, comp: C) -> usize {
        match self.free_positions.pop_front() {
            Some(x) => {
                self.cast_mut::<C>()[x] = comp;
                return x;
            }
            None => {
                let v = self.cast_mut();
                v.push(comp);
                return v.len() - 1;
            }
        }
    }

    pub fn remove<C: Component>(&mut self, index: usize) -> Option<&C> {
        if self.cast::<C>().len() > index {
            self.free_positions.push_back(index);
            return Some(&self.cast::<C>()[index]);
        } else {
            return None;
        }
    }

    pub fn get<C: Component+Copy>(&self, index: usize) -> Option<C> {
        if self.cast::<C>().len() > index {
            return Some(self.cast::<C>()[index]);
        } else {
            return None;
        }
    }

    pub fn borrow<C: Component>(&self, index: usize) -> Option<&C> {
        if self.cast::<C>().len() > index {
            return Some(&self.cast::<C>()[index]);
        } else {
            return None;
        }
    }

    pub fn borrow_mut<C: Component>(&mut self, index: usize) -> Option<&mut C> {
        if self.cast::<C>().len() > index {
            return Some(&mut self.cast_mut::<C>()[index]);
        } else {
            return None;
        }
    }

    pub fn slice<C: Component>(&self) -> &[C] {
        &self.cast::<C>()[..]
    }

    pub fn slice_mut<C: Component>(&mut self) -> &mut [C] {
        &mut self.cast_mut::<C>()[..]
    }
}

#[derive(Default)]
pub struct Fiber {
    ids: IdNumber,
    structure: HashMap<EntityId, HashMap<TypeId, usize>>,
    data: HashMap<TypeId, Storage>,
}

impl Fiber {
    pub fn new() -> Self {
        Default::default()
    }

    fn get_storage<C: Component>(&mut self) -> &mut Storage {
        let type_id = TypeId::of::<C>();
        if !self.data.contains_key(&type_id) {
            self.data.insert(type_id, Storage::new::<C>());
        }
        return self.data.get_mut(&type_id).unwrap();
    }

    pub fn memory_view<C: Component>(&self) -> EcsResult<&[u8]> {
        let type_id = TypeId::of::<C>();
        if self.data.contains_key(&type_id) {
            return Ok(self.data.get(&type_id).unwrap().get_memory_view());
        } else {
            return Err(NotFound::Component(type_id));
        }
    }

    pub fn access_all_components<C: Component>(&self) -> EcsResult<&[C]> {
        let type_id = TypeId::of::<C>();
        if !self.data.contains_key(&type_id) {
            return Err(NotFound::Component(type_id));
        } else {
            return Ok(self.data.get(&type_id).unwrap().slice());
        }
    }

    pub fn access_all_components_mut<C: Component>(&mut self) -> EcsResult<&mut [C]> {
        let type_id = TypeId::of::<C>();
        if !self.data.contains_key(&type_id) {
            return Err(NotFound::Component(type_id));
        } else {
            return Ok(self.data.get_mut(&type_id).unwrap().slice_mut());
        }
    }

    pub fn create_entity(&mut self) -> EntityId {
        let new_id = EntityId(self.ids);
        self.ids += 1;
        self.structure.insert(new_id, Default::default());
        new_id
    }

    pub fn exists(&self, id: EntityId) -> bool {
        self.structure.contains_key(&id)
    }

    pub fn list(&self) -> HashSet<EntityId> {
        return self.structure.keys().map(|k| k.clone()).collect();
    }

    pub fn list1<C: Component>(&self) -> HashSet<EntityId> {
        let type_id = TypeId::of::<C>();
        return self.structure.iter().filter_map(
            |(key, val)| if val.contains_key(&type_id)
            { Some(key.clone()) } else { None }).collect();
    }

    pub fn list2<C0: Component, C1: Component>(&self) -> HashSet<EntityId> {
        let type_id_0 = TypeId::of::<C0>();
        let type_id_1 = TypeId::of::<C1>();
        return self.structure.iter().filter_map(
            |(key, val)| if val.contains_key(&type_id_0) &&
                val.contains_key(&type_id_1) { Some(key.clone()) } else { None }).collect();
    }

    pub fn list3<C0: Component, C1: Component, C2: Component>(&self) -> HashSet<EntityId> {
        let type_id_0 = TypeId::of::<C0>();
        let type_id_1 = TypeId::of::<C1>();
        let type_id_2 = TypeId::of::<C2>();
        return self.structure.iter().filter_map(
            |(key, val)|
                if val.contains_key(&type_id_0) &&
                    val.contains_key(&type_id_1) &&
                    val.contains_key(&type_id_2) { Some(key.clone()) } else { None }).collect();
    }


    pub fn has<C: Component>(&self, id: EntityId) -> EcsResult<bool> {
        let type_id = TypeId::of::<C>();
        let entity_components = self.structure.get(&id);
        entity_components
            .map(|ec| ec.contains_key(&type_id))
            .ok_or(NotFound::Entity(id))
    }

    pub fn get<C: Component+Copy>(&self, id: EntityId) -> EcsResult<C> {
        let type_id = TypeId::of::<C>();
        let entity_components = self.structure.get(&id);
        let component_data = self.data.get(&type_id);

        match (entity_components, component_data) {
            (Some(ec), Some(cd)) => {
                if let Some(index) = ec.get(&type_id) {
                    return cd
                        .get::<C>(index.clone())
                        .ok_or(NotFound::Component(type_id));
                } else {
                    return Err(NotFound::Component(type_id));
                }
            }
            (None, _) => Err(NotFound::Entity(id)),
            (Some(_), None) => Err(NotFound::Component(type_id)),
        }
    }


    pub fn borrow<C: Component>(&self, id: EntityId) -> EcsResult<&C> {
        let type_id = TypeId::of::<C>();
        let entity_components = self.structure.get(&id);
        let component_data = self.data.get(&type_id);

        match (entity_components, component_data) {
            (Some(ec), Some(cd)) => {
                if let Some(index) = ec.get(&type_id) {
                    return cd
                        .borrow::<C>(index.clone())
                        .ok_or(NotFound::Component(type_id));
                } else {
                    return Err(NotFound::Component(type_id));
                }
            }
            (None, _) => Err(NotFound::Entity(id)),
            (Some(_), None) => Err(NotFound::Component(type_id)),
        }
    }

    pub fn borrow_mut<C: Component>(&mut self, id: EntityId) -> EcsResult<&mut C> {
        let type_id = TypeId::of::<C>();
        let entity_structure = self.structure.get(&id);
        let component_data = self.data.get_mut(&type_id);

        match (entity_structure, component_data) {
            (Some(es), Some(cd)) => {
                if let Some(index) = es.get(&type_id) {
                    return cd
                        .borrow_mut::<C>(index.clone())
                        .ok_or(NotFound::Component(type_id));
                } else {
                    return Err(NotFound::Component(type_id));
                }
            }
            (None, _) => Err(NotFound::Entity(id)),
            (Some(_), None) => Err(NotFound::Component(type_id)),
        }
    }

    pub fn borrow_mut2<C0: Component, C1: Component>(&mut self, id: EntityId) -> EcsResult<(&mut C0, &mut C1)> {
        let type_id_0 = TypeId::of::<C0>();
        let type_id_1 = TypeId::of::<C1>();
        let entity_structure = self.structure.get(&id);
        if entity_structure == None {
            return Err(NotFound::Entity(id));
        }
        let entity_structure = entity_structure.unwrap();
        let index_0 = entity_structure.get(&type_id_0);
        let index_1 = entity_structure.get(&type_id_1);

        match (index_0, index_1) {
            (Some(i0), Some(i1)) => {
                let ptr_0 = self.data.get(&type_id_0).unwrap().borrow::<C0>(i0.clone()).unwrap() as *const C0 as *mut C0;
                let ptr_1 = self.data.get(&type_id_1).unwrap().borrow::<C1>(i1.clone()).unwrap() as *const C1 as *mut C1;
                unsafe { Ok((transmute(ptr_0), transmute(ptr_1))) }
            }
            (None, _) => Err(NotFound::Component(type_id_0)),
            (_, None) => Err(NotFound::Component(type_id_1)),
        }
    }

    pub fn borrow_mut3<C0: Component, C1: Component, C2: Component>(&mut self, id: EntityId) -> EcsResult<(&mut C0, &mut C1, &mut C2)> {
        let type_id_0 = TypeId::of::<C0>();
        let type_id_1 = TypeId::of::<C1>();
        let type_id_2 = TypeId::of::<C2>();
        let entity_structure = self.structure.get(&id);
        if entity_structure == None {
            return Err(NotFound::Entity(id));
        }
        let entity_structure = entity_structure.unwrap();
        let index_0 = entity_structure.get(&type_id_0);
        let index_1 = entity_structure.get(&type_id_1);
        let index_2 = entity_structure.get(&type_id_2);


        match (index_0, index_1, index_2) {
            (Some(i0), Some(i1), Some(i2)) => {
                let ptr_0 = self.data.get(&type_id_0).unwrap().borrow::<C0>(i0.clone()).unwrap() as *const C0 as *mut C0;
                let ptr_1 = self.data.get(&type_id_1).unwrap().borrow::<C1>(i1.clone()).unwrap() as *const C1 as *mut C1;
                let ptr_2 = self.data.get(&type_id_2).unwrap().borrow::<C2>(i2.clone()).unwrap() as *const C2 as *mut C2;
                unsafe { Ok((transmute(ptr_0), transmute(ptr_1), transmute(ptr_2))) }
            }
            (None, _, _) => Err(NotFound::Component(type_id_0)),
            (_, None, _) => Err(NotFound::Component(type_id_1)),
            (_, _, None) => Err(NotFound::Component(type_id_2))
        }
    }


    pub fn set<C: Component>(&mut self, id: EntityId, comp: C) -> EcsResult<usize> {
        let type_id = TypeId::of::<C>();

        if !self.structure.contains_key(&id) {
            return Err(NotFound::Entity(id));
        }

        let component_index: Option<usize>;
        {
            component_index = self
                .structure
                .get(&id)
                .unwrap()
                .get(&type_id)
                .map(|x| x.clone());
        }

        return if let Some(comp_index) = component_index {
            *self
                .get_storage::<C>()
                .borrow_mut(comp_index.clone())
                .unwrap() = comp;
            Ok(comp_index.clone())
        } else {
            let index = self.get_storage::<C>().insert(comp);
            self.structure.get_mut(&id).unwrap().insert(type_id, index);
            Ok(index)
        };
    }
}


#[cfg(test)]
mod tests;
