# Fiber

Simple Rust Entity component system. Used in Cubicle project.

Stores component data in continuous memory and provides utility function **memory_view**, which
returns binary slice view of all component of the given type. Used for buffering of transformations
in Cubicle rendering pipeline. 

There is however problem with removed components - these are simply flagged as removed,
but still present in the memory and thus in the memory_view.